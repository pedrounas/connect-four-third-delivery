"use strict";

const crypto = require('crypto');
const fs = require('fs');
const header = require('./Header').header;
const check = require('./Check');

let responsePlayer1 = null;
let responsePlayer2 = null;
let timeout = [];

module.exports.userAlreadyCreated = function (content) {
    let user = content["nick"];
    let password = content["pass"];

    password = crypto
        .createHash('md5')
        .update(password)
        .digest('hex');


    let userList = fs.readFileSync('API/Users.json');
    userList = JSON.parse(userList.toString())["Users"];
    for (let i = 0; i < userList.length; i++) {
        if (userList[i]["nick"] == user) {
            if (userList[i]["pass"] == password) {
                return false;
            }
            return true;
        }
    }

    userList.push({ nick: user, pass: password, games: {} });
    userList = { Users: userList };

    fs.writeFileSync('API/Users.json', JSON.stringify(userList));
    return false;
}

module.exports.checkRankingPayload = function (content) {
    if (content === "{}") {
        return true;
    }
    else if (content["size"]["rows"] != parseInt(content["size"]["rows"], 10) || content["size"]["columns"] != parseInt(content["size"]["columns"], 10)) {
        return true;
    }
    return false;
}

module.exports.getRankings = function (content) {
    let userList = fs.readFileSync('API/Users.json');
    userList = JSON.parse(userList.toString())["Users"];

    let gameSize = content["size"]["rows"].toString() + "x" + content["size"]["columns"].toString();
    let results = [];

    for (let i = 0; i < userList.length; i++) {
        if (userList[i]["games"][gameSize] != null)
            results.push({ nick: userList[i]["nick"], victories: userList[i]["games"][gameSize]["victories"], games: userList[i]["games"][gameSize]["games"] });
    }

    for (let i = 0; i < results.length; i++) {
        for (let j = i + 1; j < results.length; j++) {
            if (results[j]["victories"] > results[i]["victories"]) {
                let aux = results[i];
                results[i] = results[j];
                results[j] = aux;
            }
            if (results[j]["victories"] === results[i]["victories"]) {
                if (results[j]["games"] < results[i]["games"]) {
                    let aux = results[i];
                    results[i] = results[j];
                    results[j] = aux;
                }
            }
        }
    }
    if (results.length === 0) {
        results = {};
    }
    else {
        results = { ranking: results };
    }

    return results;
}

module.exports.userAlreadyInGame = function (content) {
    let group = content['group'];
    let nick = content['nick'];
    //let gameSize = content["size"]["rows"].toString() + "x" + content["size"]["columns"].toString();
    let currentGames = fs.readFileSync('API/Games.json');
    currentGames = JSON.parse(currentGames.toString())["Games"];
    for (let i = 0; i < (currentGames).length; i++) {
        if (currentGames[i].group === group && (currentGames[i].nickPlayer1 === nick || currentGames[i].nickPlayer2 === nick)) {
            return true;
        }
    }
    return false;
}

module.exports.pairUsers = function (content) {
    let group = content['group'];
    let gameSize = content["size"]["rows"].toString() + "x" + content["size"]["columns"].toString();
    let currentGames = fs.readFileSync('API/Games.json');
    currentGames = JSON.parse(currentGames.toString())["Games"];
    for (let i = 0; i < currentGames.length; i++) {
        if (currentGames[i].group === group && currentGames[i].size === gameSize && currentGames[i].active === false) {
            return true;
        }
    }
    return false;
}

module.exports.getGame = function (content) {
    let group = content['group'];
    let gameSize = content["size"]["rows"].toString() + "x" + content["size"]["columns"].toString();
    let currentGames = fs.readFileSync('API/Games.json');
    currentGames = JSON.parse(currentGames.toString())["Games"];
    for (let i = 0; i < currentGames.length; i++) {
        if (currentGames[i].group === group && currentGames[i].size === gameSize && currentGames[i].active === false) {
            let gameID = currentGames[i].gameID;
            currentGames[i].nickPlayer2 = content["nick"];
            currentGames = { Games: currentGames };
            fs.writeFileSync('API/Games.json', JSON.stringify(currentGames));
            return gameID;
        }
    }
    return null;
}

module.exports.createGame = function (content) {
    let currentDate = new Date().getTime().toString();
    let gameSize = content["size"]["rows"].toString() + "x" + content["size"]["columns"].toString();
    let to = setTimeout(function () {
        addressTimeout(gameID);
    }, 60000);
    timeout.push(to);
    currentDate = currentDate + gameSize;
    let gameID = crypto
        .createHash('md5')
        .update(currentDate)
        .digest('hex');

    let newBoard = new Array(content["size"]["rows"]);
    for (let i = 0; i < content["size"]["rows"]; i++) {
        newBoard[i] = new Array(content["size"]["columns"]);
        for (let j = 0; j < content["size"]["columns"]; j++) {
            newBoard[i][j] = null;
        }
    }
    let currentGames = fs.readFileSync('API/Games.json');
    currentGames = JSON.parse(currentGames.toString())["Games"];
    currentGames.push({ group: content["group"], size: gameSize, nickPlayer1: content['nick'], nickPlayer2: null, gameID: gameID, board: newBoard, turn: content['nick'], active: false });
    currentGames = { Games: currentGames };
    fs.writeFileSync('API/Games.json', JSON.stringify(currentGames));
    return gameID;
}

module.exports.isValidGame = function (content) {
    let gameID = content["game"];
    let nick = content["nick"];
    let currentGames = fs.readFileSync('API/Games.json');
    currentGames = JSON.parse(currentGames.toString())["Games"];

    for (let i = 0; i < currentGames.length; i++) {
        if (currentGames[i].gameID === gameID) {
            if (nick === currentGames[i].nickPlayer1) {
                updateTable(currentGames[i], currentGames[i].nickPlayer2);
                update(JSON.stringify({ winner: currentGames[i].nickPlayer2 }), responsePlayer1, responsePlayer2);
            }
            else {
                updateTable(currentGames[i], currentGames[i].nickPlayer1);
                update(JSON.stringify({ winner: currentGames[i].nickPlayer1 }), responsePlayer1, responsePlayer2);
            }
            responsePlayer1 = null;
            responsePlayer2 = null;
            currentGames.splice(i, 1);
            currentGames = { Games: currentGames };
            fs.writeFileSync('API/Games.json', JSON.stringify(currentGames));
            return true;
        }
    }
    return false;
}

module.exports.notify = function (content) {
    let gameID = content["game"];
    let nick = content["nick"];
    let column = content["column"];
    let currentGames = fs.readFileSync('API/Games.json');
    currentGames = JSON.parse(currentGames.toString())["Games"];
    for (let i = 0; i < currentGames.length; i++) {
        if (currentGames[i].gameID == gameID && currentGames[i].active === true) {
            if (currentGames[i].turn != nick)
                return 1;
            else if (column < 0)
                return 2;
            else {
                clearTimeout(timeout[i]);
                let rows = currentGames[i].size.charAt(0);
                let move = findEmpty(column, nick, currentGames[i].board, rows);
                if (move === -1)
                    return 3;
                else {
                    currentGames[i].board[move][column] = nick;
                    let checkWinner = check.checkWinner(currentGames[i]);
                    if (checkWinner === 3) { //Empate
                        update(JSON.stringify({ column: column, winner: null, board: currentGames[i].board }), responsePlayer1, responsePlayer2);
                        responsePlayer1.end();
                        responsePlayer2.end();
                        updateTable(currentGames[i], null);
                        responsePlayer1 = null;
                        responsePlayer2 = null;
                        timeout.splice(i, 1);
                        currentGames.splice(i, 1);
                        currentGames = { Games: currentGames };
                        fs.writeFileSync('API/Games.json', JSON.stringify(currentGames));
                    }
                    else if (checkWinner === 1) { //Jogador 1
                        update(JSON.stringify({ column: column, winner: currentGames[i].nickPlayer1, board: currentGames[i].board }), responsePlayer1, responsePlayer2);
                        responsePlayer1.end();
                        responsePlayer2.end();
                        updateTable(currentGames[i], currentGames[i].nickPlayer1);
                        responsePlayer1 = null;
                        responsePlayer2 = null;
                        currentGames.splice(i, 1);
                        currentGames = { Games: currentGames };
                        fs.writeFileSync('API/Games.json', JSON.stringify(currentGames));
                    }
                    else if (checkWinner === 2) { //Jogador 2
                        update(JSON.stringify({ column: column, winner: currentGames[i].nickPlayer2, board: currentGames[i].board }), responsePlayer1, responsePlayer2);
                        responsePlayer1.end();
                        responsePlayer2.end();
                        updateTable(currentGames[i], currentGames[i].nickPlayer2);
                        responsePlayer1 = null;
                        responsePlayer2 = null;
                        currentGames.splice(i, 1);
                        currentGames = { Games: currentGames };
                        fs.writeFileSync('API/Games.json', JSON.stringify(currentGames));
                    }
                    else if (checkWinner === 0) {
                        if (nick === currentGames[i].nickPlayer1)
                            currentGames[i].turn = currentGames[i].nickPlayer2;
                        else {
                            currentGames[i].turn = currentGames[i].nickPlayer1;
                        }
                        timeout[i] = setTimeout(function () {
                            addressTimeout(gameID);
                        }, 60000);
                        update(JSON.stringify({ column: column, turn: currentGames[i].turn, board: currentGames[i].board }), responsePlayer1, responsePlayer2);
                        currentGames = { Games: currentGames };
                        fs.writeFileSync('API/Games.json', JSON.stringify(currentGames));
                    }
                    return 4;
                }
            }
        }

    }
}

function findEmpty(column, nick, board, rows) {
    for (let i = rows - 1; i >= 0; i--) {
        if (board[i][column] === null)
            return i;
    }
    return -1;
}

module.exports.checkGameID = function (content, response) {
    let gameID = content["game"];
    let nick = content["nick"];
    let currentGames = fs.readFileSync('API/Games.json');
    currentGames = JSON.parse(currentGames.toString())["Games"];
    for (let i = 0; i < currentGames.length; i++) {
        if (currentGames[i].gameID === gameID) {
            if (currentGames[i].nickPlayer1 === nick && responsePlayer1 === null) {
                responsePlayer1 = response;
                response.writeHead(200, header['sse']);
                currentGames = { Games: currentGames };
                fs.writeFileSync('API/Games.json', JSON.stringify(currentGames));
                return true;
            }
            else if (currentGames[i].nickPlayer2 === nick && responsePlayer2 === null) {
                responsePlayer2 = response;
                response.writeHead(200, header['sse']);
                currentGames[i].active = true;
                update(JSON.stringify({ turn: currentGames[i].turn, board: currentGames[i].board }), responsePlayer1, responsePlayer2);
                currentGames = { Games: currentGames };
                fs.writeFileSync('API/Games.json', JSON.stringify(currentGames));
                return true;
            }
        }
        break;
    }
    return false;
}

function updateTable(content, winner) {
    let size = content.size;
    let player1 = content.nickPlayer1;
    let player2 = content.nickPlayer2;
    let userList = fs.readFileSync('API/Users.json');
    userList = JSON.parse(userList.toString())["Users"];
    if (winner === null) {
        for (let i = 0; i < userList.length; i++) {
            if (player1 === userList[i].nick) {
                if (userList[i]["games"][size] === undefined) {
                    userList[i]["games"][size] = {};
                    userList[i]["games"][size]["games"] = 1;
                }

            }
            if (player2 === userList[i].nick) {
                if (userList[i]["games"][size] === undefined) {
                    userList[i]["games"][size] = {};
                    userList[i]["games"][size]["games"] = 1;
                }
            }
        }
    }
    else if (winner === player1) {
        for (let i = 0; i < userList.length; i++) {
            if (player1 === userList[i].nick) {
                if (userList[i]["games"][size] === undefined) {
                    userList[i]["games"][size] = {};
                    userList[i]["games"][size]["victories"] = 1;
                    userList[i]["games"][size]["games"] = 1;
                }
                else {
                    userList[i]["games"][size]["victories"]++;
                    userList[i]["games"][size]["games"]++;
                }
            }
            if (player2 === userList[i].nick) {
                if (userList[i]["games"][size] === undefined) {
                    userList[i]["games"][size] = {};
                    userList[i]["games"][size]["victories"] = 0;
                    userList[i]["games"][size]["games"] = 1;
                }
                else {
                    userList[i]["games"][size]["games"]++;
                }
            }
        }
    }
    else {
        for (let i = 0; i < userList.length; i++) {
            if (player1 === userList[i].nick) {
                if (userList[i]["games"][size] === undefined) {
                    userList[i]["games"][size] = {};
                    userList[i]["games"][size]["victories"] = 0;
                    userList[i]["games"][size]["games"] = 1;
                }
                else {
                    userList[i]["games"][size]["games"]++;
                }
            }
            if (player2 === userList[i].nick) {
                if (userList[i]["games"][size] === undefined) {
                    userList[i]["games"][size] = {};
                    userList[i]["games"][size]["victories"] = 1;
                    userList[i]["games"][size]["games"] = 1;
                }
                else {
                    userList[i]["games"][size]["victories"]++;
                    userList[i]["games"][size]["games"]++;
                }
            }
        }
    }
    userList = { Users: userList };
    fs.writeFileSync('API/Users.json', JSON.stringify(userList));
}

function update(message, responsePlayer1, responsePlayer2) {
    if (responsePlayer1 != null) {
        responsePlayer1.write('data: ' + message + '\n\n');
    }
    if (responsePlayer2 != null) {
        responsePlayer2.write('data: ' + message + '\n\n');
    }
}

function addressTimeout(gameID) {
    let currentGames = fs.readFileSync('API/Games.json');
    currentGames = JSON.parse(currentGames.toString())["Games"];

    for (var i = 0; i < currentGames.length; i++) {
        if (currentGames[i].gameID == gameID) {
            if (currentGames[i].nickPlayer2 == null)
                update(JSON.stringify({ winner: null }), responsePlayer1, responsePlayer2);
            else if (currentGames[i].turn == currentGames[i].nickPlayer1)
                update(JSON.stringify({ winner: currentGames[i].nickPlayer2 }), responsePlayer1, responsePlayer2);
            else
                update(JSON.stringify({ winner: currentGames[i].nickPlayer1 }), responsePlayer1, responsePlayer2);

            if (responsePlayer1 != null) {
                responsePlayer1.end();
            }
            if (responsePlayer2 != null) {
                responsePlayer2.end();
            }
            timeout.splice(i, 1);
            currentGames.splice(i, 1);
            responsePlayer1 = null;
            responsePlayer2 = null;
            currentGames = { Games: currentGames };
            fs.writeFileSync('API/Games.json', JSON.stringify(currentGames));
            break;
        }
    }
}
