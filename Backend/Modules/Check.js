"use strict";

module.exports.checkWinner = function (content) {
    let board = content.board;
    let player1 = content.nickPlayer1;
    let player2 = content.nickPlayer2;
    let rows = content.size.charAt(0);
    let columns = content.size.slice(2);

    if (checkTie(board, rows, columns)) {
        return 3;
    }
    else {
        //Horizontal
        for (let i = 0; i < rows; i++) {
            for (let j = 0; j < columns - 3; j++) {
                let cell = board[i][j];
                let cell2 = board[i][j+1];
                let cell3 = board[i][j+2];
                let cell4 = board[i][j+3];
                if (cell === player1 && cell2 === player1 && cell3 === player1 && cell4 === player1) {
                    return 1;
                }
                if (cell === player2 && cell2 === player2 && cell3 === player2 && cell4 === player2) {
                    return 2;
                }
            }
        }
        //Vertical
        for (let i = rows-1; i >= 3 ; i--) {
            for (let j = 0; j < columns; j++) {
                let cell = board[i][j];
                let cell2 = board[i-1][j];
                let cell3 = board[i-2][j];
                let cell4 = board[i-3][j];
                if (cell === player1 && cell2 === player1 && cell3 === player1 && cell4 === player1) {
                    return 1;
                }
                if (cell === player2 && cell2 === player2 && cell3 === player2 && cell4 === player2) {
                    return 2;
                }
            }
        }
        //Diagonal 1
        for (let i = rows-1; i >= 3 ; i--) {
            for (let j = 0; j < columns -3 ; j++) {
                let cell = board[i][j];
                let cell2 = board[i-1][j+1];
                let cell3 = board[i-2][j+2];
                let cell4 = board[i-3][j+3];
                if (cell === player1 && cell2 === player1 && cell3 === player1 && cell4 === player1) {
                    return 1;
                }
                if (cell === player2 && cell2 === player2 && cell3 === player2 && cell4 === player2) {
                    return 2;
                }
            }
        }
        //Diagonal 2
        for (let i = 0; i < rows - 3 ; i++) {
            for (let j = 0; j < columns -3 ; j++) {
                let cell = board[i][j];
                let cell2 = board[i+1][j+1];
                let cell3 = board[i+2][j+2];
                let cell4 = board[i+3][j+3];
                if (cell === player1 && cell2 === player1 && cell3 === player1 && cell4 === player1) {
                    return 1;
                }
                if (cell === player2 && cell2 === player2 && cell3 === player2 && cell4 === player2) {
                    return 2;
                }
            }
        }
    }
    return 0;
}

function checkTie(board,rows,columns) {
    for (let i = 0; i < rows; i++) {
        for (let j = 0; j < columns; j++) {
            if (board[i][j] === null) {
                return false;
            }
        }
    }
    return true;
}