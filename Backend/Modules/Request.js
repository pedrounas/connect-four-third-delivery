"use strict";

const url = require('url');
const fs = require("fs");
const crypto = require('crypto');
const header = require('./Header').header;
const helper = require('./Helper');

module.exports.getRequest = function (request, response) {
  let sentURL = url.parse(request.url, true);
  let path = sentURL.pathname;
  let content = sentURL.query;
  let body = '';

  request.on('data', function (chunk) {
    body += chunk;
  });

  request.on('end', function () {
    switch (path) {
      case '/': //THIS PART WORKS
        fs.readFile('../Frontend/index.html', function (err, data) {
          if (err) throw err;
          response.writeHead(200, header['html']);
          response.write(data);
          response.end();
        });
        break;
      case '/style.css':
        fs.readFile('../Frontend/style.css', function (err, data) {
          if (err) throw err;
          response.writeHead(200, header['css']);
          response.write(data);
          response.end();
        });
        break;
      case '/main.js':
        fs.readFile('../Frontend/main.js', function (err, data) {
          if (err) throw err;
          response.writeHead(200, header['plain']);
          response.write(data);
          response.end();
        });
        break;
      case '/update':
        if (content["game"] == null) {
          response.writeHead(400, header['plain']);
          response.write(JSON.stringify({ error: "Game is undefined" }));
          response.end();
          break;
        }

        else if (content["nick"] == null) {
          response.writeHead(400, header['plain']);
          response.write(JSON.stringify({ error: "Nick is undefined" }));
          response.end();
          break;
        }
        if (!helper.checkGameID(content, response)) {
          response.writeHead(400, header['plain']);
          response.write(JSON.stringify({ error: "Invalid game reference" }));
          response.end();
        }
        break;
      case '/assets/icon.png':
        fs.readFile('Assets/icon.png', function (err, data) {
          if (err) throw err;
          response.writeHead(200, header['image']);
          response.write(data);
          response.end();
        });
        break;
      case '/assets/logo.png':
        fs.readFile('Assets/logo.png', function (err, data) {
          if (err) throw err;
          response.writeHead(200, header['image']);
          response.write(data);
          response.end();
        });
        break;
      case '/assets/qbkls.png':
        fs.readFile('Assets/qbkls.png', function (err, data) {
          if (err) throw err;
          response.writeHead(200, header['image']);
          response.write(data);
          response.end();
        });
        break;
      default:
        response.writeHead(404, header['plain']);
        response.end();
        break;
    }
  })

  request.on('close', function (err) {
    response.end();
  });
  request.on('error', function (err) {
    console.log(err.message);
    response.writeHead(400, headers['plain']);
    response.end();
  });
}

module.exports.postRequest = function (request, response) {
  let sentURL = url.parse(request.url, true);
  let path = sentURL.pathname;
  let body = '';

  request.on('data', function (chunk) {
    body += chunk;
  });

  request.on('end', function () {
    try {
      var content = JSON.parse(body);
    }
    catch (e) {
      console.log(e.message);
      response.writeHead(400, header['plain']);
      response.write(JSON.stringify({ error: "Error parsing JSON request: " + err }));
      response.end();
      return;
    }
    switch (path) {
      case '/register':
        if (content["nick"] == null) {
          response.writeHead(400, header['plain']);
          response.write(JSON.stringify({ error: "Nick is undefined" }));
          response.end();
          break;
        }
        else if (content["pass"] == null) {
          response.writeHead(400, header['plain']);
          response.write(JSON.stringify({ error: "Pass is undefined" }));
          response.end();
          break;
        }
        var res = checkUserdata(content["nick"], content["pass"]);

        if (res == 2) {
          response.writeHead(500, header['plain']);
          response.end()
        }
        if (helper.userAlreadyCreated(content)) {
          response.writeHead(400, header['plain']);
          response.write(JSON.stringify({ error: "User registered with a different password" }));
          response.end();
        }
        else {
          response.writeHead(200, header['plain']);
          response.write(JSON.stringify({}));
          response.end();
        }
        break;

      case '/ranking':
        if (helper.checkRankingPayload(content)) {
          if (content === "{}") {
            response.writeHead(400, header['plain']);
            response.write(JSON.stringify({ error: "Undefined size" }));
            response.end();
          }
          else {
            response.writeHead(400, header['plain']);
            response.write(JSON.stringify({ error: "Invalid size" }));
            response.end();
          }
        }
        else {
          let results = helper.getRankings(content);
          response.writeHead(200, header['plain']);
          response.write(JSON.stringify(results));
          response.end();
        }
        break;

      case '/join':
        if (content["group"] == null) {
          response.writeHead(400, header['plain']);
          response.write(JSON.stringify({ error: "Group is undefined" }));
          response.end();
          break;
        }
        else if (content["nick"] == null) {
          response.writeHead(400, header['plain']);
          response.write(JSON.stringify({ error: "Nick is undefined" }));
          response.end();
          break;
        }
        else if (content["pass"] == null) {
          response.writeHead(400, header['plain']);
          response.write(JSON.stringify({ error: "Pass is undefined" }));
          response.end();
          break;
        }
        else if (content["size"] == null) {
          response.writeHead(400, header['plain']);
          response.write(JSON.stringify({ error: "Size is undefined" }));
          response.end();
          break;
        }

        var res = checkUserdata(content["nick"], content["pass"]);

        if (res == 1) {
          response.writeHead(400, header['plain']);
          response.write(JSON.stringify({ error: "User registered with a different password" }));
          response.end();
          break;
        }

        else if (res == 2) {
          response.writeHead(500, header['plain']);
          response.end()
        }
        if (helper.userAlreadyInGame(content)) {
          response.writeHead(400, header['plain']);
          response.write(JSON.stringify({ error: "Already waiting for a game" }));
          response.end();
        }
        else if (helper.pairUsers(content)) {
          let gameID = helper.getGame(content);
          if (gameID != null) {
            response.writeHead(200, header['plain']);
            response.write(JSON.stringify({ game: gameID }));
            response.end();
          }
          else {
            let gameID = helper.createGame(content);
            response.writeHead(200, header['plain']);
            response.write(JSON.stringify({ game: gameID }));
            response.end();
          }
        }
        else {
          let gameID = helper.createGame(content);
          response.writeHead(200, header['plain']);
          response.write(JSON.stringify({ game: gameID }));
          response.end();
        }
        break;

      case '/leave':
        if (content["nick"] == null) {
          response.writeHead(400, header['plain']);
          response.write(JSON.stringify({ error: "Nick is undefined" }));
          response.end();
          break;
        }
        else if (content["pass"] == null) {
          response.writeHead(400, header['plain']);
          response.write(JSON.stringify({ error: "Pass is undefined" }));
          response.end();
          break;
        }
        else if (content["game"] == null) {
          response.writeHead(400, header['plain']);
          response.write(JSON.stringify({ error: "Game is undefined" }));
          response.end();
          break;
        }

        var res = checkUserdata(content["nick"], content["pass"]);

        if (res == 1) {
          response.writeHead(400, header['plain']);
          response.write(JSON.stringify({ error: "User registered with a different password" }));
          response.end();
          break;
        }

        else if (res == 2) {
          response.writeHead(500, header['plain']);
          response.end()
        }

        if (!helper.isValidGame(content)) {
          response.writeHead(400, header['plain']);
          response.write(JSON.stringify({ error: "Invalid game reference" }));
          response.end();
        }
        else {
          response.writeHead(200, header['plain']);
          response.write(JSON.stringify({}));
          response.end();
        }
        break;

      case '/notify':

        if (content["game"] == null) {
          response.writeHead(400, header['plain']);
          response.write(JSON.stringify({ error: "Game is undefined" }));
          response.end();
          break;
        }
        else if (content["nick"] == null) {
          response.writeHead(400, header['plain']);
          response.write(JSON.stringify({ error: "Nick is undefined" }));
          response.end();
          break;
        }
        else if (content["pass"] == null) {
          response.writeHead(400, header['plain']);
          response.write(JSON.stringify({ error: "Pass is undefined" }));
          response.end();
          break;
        }

        var res = checkUserdata(content["nick"], content["pass"]);

        if (res == 1) {
          response.writeHead(400, content['plain']);
          response.write(JSON.stringify({ error: "User registered with a different password" }));
          response.end();
          break;
        }

        else if (res == 2) {
          response.writeHead(500, header['plain']);
          response.end()
        }
        if (helper.notify(content) === 1) {
          response.writeHead(400, header['plain']);
          response.write(JSON.stringify({ error: "Not your turn to play" }));
          response.end();
        }
        else if (helper.notify(content) === 2) {
          response.writeHead(400, header['plain']);
          response.write(JSON.stringify({ error: "Column reference is negative" }));
          response.end();
        }
        else if (helper.notify(content) === 3) {
          response.writeHead(400, header['plain']);
          response.write(JSON.stringify({ error: "Column is full" }));
          response.end();
        }
        else {
          response.writeHead(200, header['plain']);
          response.write(JSON.stringify({}));
          response.end();
        }
        break;
      default:
        response.writeHead(404, header['plain']);
        response.end();
        break;
    }
  });
}

function checkUserdata(nick, pass) {
  if (nick == "" || pass == "") {
    return 1;
  }

  pass = crypto
    .createHash('md5')
    .update(pass)
    .digest('hex');

  try {
    var fileData = fs.readFileSync("API/Users.json");
    fileData = JSON.parse(fileData.toString())["Users"];
  }
  catch (err) {
    console.log(err);
    return 2;
  }

  var found = false;
  var i;

  for (i = 0; i < fileData.length; i++) {
    if (fileData[i]["nick"] == nick) {
      found = true;
      break;
    }
  }
  if (found == false) {
    fileData.push({ nick: nick, pass: pass, games: {} });
    fileData = { Users: fileData };
    try {
      fs.writeFileSync("API/Users.json", JSON.stringify(fileData));
    }
    catch (err) {
      console.log(err);
      return 2;
    }
  }
  else {
    if (fileData[i]["pass"] == pass) {
      return 0;
    }
    else
      return 1;
  }
}
